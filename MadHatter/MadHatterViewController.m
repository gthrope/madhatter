//
//  MadHatterViewController.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/3/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "MadHatterViewController.h"
#import "BuildItViewController.h"

@interface MadHatterViewController ()

@end

@implementation MadHatterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSelectedIndex:1];
}

@end
