//
//  MadHatterAppDelegate.h
//  MadHatter
//
//  Created by Glenn Thrope on 5/3/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MadHatterAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
