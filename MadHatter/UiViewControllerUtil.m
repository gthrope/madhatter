//
//  UiViewControllerUtil.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "UiViewControllerUtil.h"

@implementation UiViewControllerUtil

+ (void)popOneLevelFromViewController:(UIViewController *)viewController {
    UIViewController *oneLevelUp = [viewController.navigationController.viewControllers objectAtIndex:(viewController.navigationController.viewControllers.count - 2)];
    [viewController.navigationController popToViewController:oneLevelUp animated:YES];
}

+ (NSString *)colorNumberToString:(NSInteger)color {
    NSString *returnValue = @"";
    switch(color) {
        case(0):
            returnValue = @"Red";
            break;
        case(1):
            returnValue = @"Orange";
            break;
        case(2):
            returnValue = @"Yellow";
            break;
        case(3):
            returnValue = @"Green";
            break;
        case(4):
            returnValue = @"Blue";
            break;
        case(5):
            returnValue = @"Purple";
            break;
    }
    return returnValue;
}

// fix these colors, they're not quite right
+ (UIColor *)colorNumberToUIColor:(NSInteger)color {
    UIColor *returnValue;
    switch(color) {
        case(0):
            returnValue = [UIColor redColor];
            break;
        case(1):
            returnValue = [UIColor orangeColor];
            break;
        case(2):
            returnValue = [UIColor yellowColor];
            break;
        case(3):
            returnValue = [UIColor greenColor];
            break;
        case(4):
            returnValue = [UIColor blueColor];
            break;
        case(5):
            returnValue = [UIColor purpleColor];
            break;
    }
    return returnValue;
}


@end
