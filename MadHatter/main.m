//
//  main.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/3/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MadHatterAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MadHatterAppDelegate class]));
    }
}
