//
//  BuyItViewController.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "BuyItViewController.h"
#import "UiViewControllerUtil.h"

@interface BuyItViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *hatImage;
@property (weak, nonatomic) IBOutlet UILabel *hatLabel;
@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;
@property (weak, nonatomic) IBOutlet UIButton *standard;
@property (weak, nonatomic) IBOutlet UIButton *premium;
@property (weak, nonatomic) IBOutlet UIButton *express;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (strong, nonatomic) NSDecimalNumber *totalAmount;

@end

@implementation BuyItViewController

@synthesize customHat = _customHat;
@synthesize totalAmount = _totalAmount;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.hatImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", self.customHat.hatColor]];
    self.hatLabel.text = self.customHat.content;
    // note the colors here aren't quite right
    self.hatLabel.textColor = [UiViewControllerUtil colorNumberToUIColor:self.customHat.textColor];
    
}

- (IBAction)shippingClicked:(UIButton *)sender {
    [self.standard setTitleColor:[UIColor lightGrayColor] forState: UIControlStateNormal];
    [self.premium setTitleColor:[UIColor lightGrayColor] forState: UIControlStateNormal];
    [self.express setTitleColor:[UIColor lightGrayColor] forState: UIControlStateNormal];

    [sender setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];

    if(sender == self.standard) {
        self.totalAmount = [[NSDecimalNumber alloc] initWithDouble:15.99];
    }
    if(sender == self.premium) {
        self.totalAmount = [[NSDecimalNumber alloc] initWithDouble:21.99];
    }
    if(sender == self.express) {
        self.totalAmount = [[NSDecimalNumber alloc] initWithDouble:28.99];
    }
    
    self.totalAmountLabel.text = [NSString stringWithFormat:@"Total: $%.02f", self.totalAmount.floatValue];
    
}


- (IBAction)goBackPressed {
    [UiViewControllerUtil popOneLevelFromViewController:self];
}

- (BuyItViewController *)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _payPalConfiguration = [[PayPalConfiguration alloc] init];
        
        // See PayPalConfiguration.h for details and default values.
        // Should you wish to change any of the values, you can do so here.
        // For example, if you wish to accept PayPal but not payment card payments, then add:
        _payPalConfiguration.acceptCreditCards = NO;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Start out working with the test environment! When you are ready, switch to PayPalEnvironmentProduction.
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
}


// Don't let user buy unless they've selected shipping means!

- (IBAction)pay {
    
    // Create a PayPalPayment
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    // Amount, currency, and description
    payment.amount = self.totalAmount;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Mad Hatter cap"; // need some kind of hat model, so I can get order info here
    
    // Use the intent property to indicate that this is a "sale" payment,
    // meaning combined Authorization + Capture. To perform Authorization only,
    // and defer Capture to your server, use PayPalPaymentIntentAuthorize.
    payment.intent = PayPalPaymentIntentSale;
    
    // Check whether payment is processable.
    if (!payment.processable) {
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
    }
    
    // Create a PayPalPaymentViewController.
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                   configuration:self.payPalConfiguration
                                                                        delegate:self];
    
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController
                 didCompletePayment:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    // Send the entire confirmation dictionary
    //NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
    //                                                       options:0
    //                                                         error:nil];
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
    
    [self performSegueWithIdentifier:@"thankYouSegue" sender:self];
}

@end
