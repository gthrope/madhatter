//
//  BuildItViewController.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "BuildItViewController.h"
#import "BuyItViewController.h"
#import "UiViewControllerUtil.h"
#import "CustomHat.h"

@interface BuildItViewController ()
@property (strong, nonatomic) CustomHat *customHat;
@property (weak, nonatomic) IBOutlet UIImageView *previewView;
@property (weak, nonatomic) IBOutlet UITextView *hatTextView;
@property (nonatomic) BOOL firstTimeEdit;

@end

@implementation BuildItViewController

@synthesize customHat = _customHat;

- (CustomHat *)customHat {
    if(!_customHat) {
        _customHat = [[CustomHat alloc] init];
    }
    return _customHat;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.firstTimeEdit = YES;
}

- (IBAction)hatColorPressed:(UIButton *)sender {
    self.previewView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", sender.tag]];
    self.customHat.hatColor = sender.tag;
}

- (IBAction)fontColorPressed:(UIButton *)sender {
    self.hatTextView.textColor = sender.backgroundColor;
    self.customHat.textColor = sender.tag;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if(self.firstTimeEdit) {
        textView.text = @"";
        self.firstTimeEdit = NO;
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    if([self.hatTextView.text isEqualToString:@""]) {
        self.hatTextView.text = @"Enter text here...";
    }

    // if user didn't enter anything, we pass "Enter text here"
    self.customHat.content = self.hatTextView.text;
}

- (IBAction)goBack {
    [UiViewControllerUtil popOneLevelFromViewController:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"buyItSegue"]) {
        BuyItViewController *buyItViewController = segue.destinationViewController;
        buyItViewController.customHat = self.customHat;
    }
}

@end
