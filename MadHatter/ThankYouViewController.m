//
//  ThankYouViewController.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "ThankYouViewController.h"

@interface ThankYouViewController ()

@end

@implementation ThankYouViewController

- (IBAction)startOver {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
