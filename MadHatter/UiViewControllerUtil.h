//
//  UiViewControllerUtil.h
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UiViewControllerUtil : NSObject

+ (void)popOneLevelFromViewController:(UIViewController *)viewController;
+ (NSString *)colorNumberToString:(NSInteger)color;
+ (UIColor *)colorNumberToUIColor:(NSInteger)color;

@end
