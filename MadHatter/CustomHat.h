//
//  CustomHat.h
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomHat : NSObject

@property (strong, nonatomic) NSString *content;
@property (nonatomic) NSInteger hatColor;
@property (nonatomic) NSInteger textColor;

@end
