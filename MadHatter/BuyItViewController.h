//
//  BuyItViewController.h
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "CustomHat.h"

@interface BuyItViewController : UIViewController<PayPalPaymentDelegate>
// should I be synthesizing this?
@property (strong, nonatomic) CustomHat *customHat;
@end
