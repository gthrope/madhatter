//
//  CustomHat.m
//  MadHatter
//
//  Created by Glenn Thrope on 5/4/14.
//  Copyright (c) 2014 MadHatter. All rights reserved.
//

#import "CustomHat.h"

@implementation CustomHat

@synthesize content = _content;
@synthesize hatColor = _hatColor;
@synthesize textColor = _textColor;

@end
